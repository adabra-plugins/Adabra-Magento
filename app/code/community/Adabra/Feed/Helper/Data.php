<?php
/**
 * MageSpecialist
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magespecialist.it so we can send you a copy immediately.
 *
 * @category   Adabra
 * @package    Adabra_Feed
 * @copyright  Copyright (c) 2017 Skeeller srl / MageSpecialist (http://www.magespecialist.it)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Adabra_Feed_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_BATCH_SIZE = 'adabra_feed/batch_size';
    const XML_PATH_ORDER_STATES = 'adabra_feed/order/states';
    const XML_PATH_ORDER_DAY_INTERVAL = 'adabra_feed/order/export_days';
    const XML_PATH_ORDER_EXCLUDE_ORDER = 'adabra_feed/order/exclude_order';
    const XML_PATH_ORDER_EXCLUDE_ORDER_FILTER_LIST = 'adabra_feed/order/filter_list';

    const XML_PATH_GENERAL_CRON = 'adabra_feed/general/use_cron';
    const XML_PATH_GENERAL_COMPRESS = 'adabra_feed/general/compress';
    const XML_PATH_GENERAL_IMAGE_TYPE = 'adabra_feed/general/image_type';
    const XML_PATH_GENERAL_IMAGE_SIZE = 'adabra_feed/general/image_size';
    const XML_PATH_GENERAL_REBUILD_TIME = 'adabra_feed/general/rebuild_time';
    const XML_PATH_GENERAL_INCREMENTAL_ENABLED = 'adabra_feed/general/incremental_enabled';

    const XML_PATH_GENERAL_API_NOTIFY = 'adabra_feed/general/api_notify';
    const XML_PATH_GENERAL_API_STAGING_MODE = 'adabra_feed/general/api_staging_mode';

    const XML_PATH_GENERAL_ENABLE_LOGGING_MODE = 'adabra_feed/general/enable_logging_mode';
    const ADABRA_LOG = 'adabra.log';

    const XML_PATH_GENERAL_CUSTOM_TAGS = 'adabra_feed/custom_tags/custom_tags_list';

    const XML_PATH_HTTP_ENABLED = 'adabra_feed/http/enabled';
    const XML_PATH_HTTP_USER = 'adabra_feed/http/user';
    const XML_PATH_HTTP_PASS = 'adabra_feed/http/pass';

    const XML_PATH_FTP_ENABLED = 'adabra_feed/ftp/enabled';
    const XML_PATH_FTP_USER = 'adabra_feed/ftp/user';
    const XML_PATH_FTP_PASS = 'adabra_feed/ftp/pass';
    const XML_PATH_FTP_HOST = 'adabra_feed/ftp/host';
    const XML_PATH_FTP_PATH = 'adabra_feed/ftp/path';
    const XML_PATH_FTP_PORT = 'adabra_feed/ftp/port';
    const XML_PATH_FTP_SSL = 'adabra_feed/ftp/ssl';
    const XML_PATH_FTP_PASSIVE = 'adabra_feed/ftp/passive';

    const XML_PATH_CUSTOMER_ENABLED = 'adabra_feed/customer/enabled';
    const XML_PATH_CUSTOMER_ADDRESS_TYPE = 'adabra_feed/customer/address_type';

    const XML_PATH_NEWSLETTER_ENABLED = 'adabra_feed/newsletter/enabled';

    /* parametri per chiamate API */
    const API_MAGENTO_SIGNATURE_KEY = 'Twdw%!wH$J=m@q@J?bVGR4_g=ZPCruGq^8h@VLnE!qn#tm^G&z2&u*w2QMznU8!W';
    const RELATIVE_ENDPOINT = 'api/magento/import/';
    const BASE_URL_STAGING = 'http://staging.marketingspray.com/';
    const BASE_URL_PRODUCTION = 'https://my.adabra.com/';

    const CATALOG_ACTION = 'catalog';
    const CUSTOMER_ACTION = 'customer';
    const ORDER_ACTION = 'order';
    const NEWSLETTER_ACTION = 'newsletter';

    protected $_rootPathIds = array();

    /**
     * Return true when products batch mode is enabled
     * @param string $type
     * @return bool
     */
    public function isBatchEnabled($type)
    {
        return ($this->getBatchSize($type) > 0);
    }

    /**
     * Get products batch size
     * @param string $type
     * @return int
     */
    public function getBatchSize($type)
    {
        $xmlPath = static::XML_PATH_BATCH_SIZE.'/'.$type;
        return intval(Mage::getStoreConfig($xmlPath));
    }

    /**
     * Get products batch size
     * @return bool
     */
    public function getCompress()
    {
        return (bool) Mage::getStoreConfig(static::XML_PATH_GENERAL_COMPRESS);
    }

    /**
     * Get image type
     * @return string
     */
    public function getImageType()
    {
        return Mage::getStoreConfig(static::XML_PATH_GENERAL_IMAGE_TYPE);
    }

    /**
     * Get image size
     * @return string
     */
    public function getImageSize()
    {
        return intval(Mage::getStoreConfig(static::XML_PATH_GENERAL_IMAGE_SIZE));
    }

    /**
     * Get order states for export
     * @return array
     */
    public function getOrderStates()
    {
        return explode(',', Mage::getStoreConfig(self::XML_PATH_ORDER_STATES));
    }

    /**
     * Get days interval to order export
     * @return array
     */
    public function getDayIntervalExport()
    {
        return  Mage::getStoreConfig(self::XML_PATH_ORDER_DAY_INTERVAL);
    }

    /**
     * Return true if cron mode is enabled
     * @return bool
     */
    public function isCronEnabled()
    {
        return (bool) Mage::getStoreConfig(self::XML_PATH_GENERAL_CRON);
    }

    /**
     * Get rebuild time
     * @return array
     */
    public function getRebuildTime()
    {
        return explode(',', Mage::getStoreConfig(self::XML_PATH_GENERAL_REBUILD_TIME));
    }

    /**
     * Is http download enabled
     * @return bool
     */
    public function isHttpEnabled()
    {
        if (!Mage::getStoreConfig(self::XML_PATH_HTTP_ENABLED)) {
            return false;
        }

        return ($this->getHttpAuthUser() && $this->getHttpAuthPassword());
    }

    /**
     * Get auth username
     * @return string
     */
    public function getHttpAuthUser()
    {
        return trim(Mage::getStoreConfig(self::XML_PATH_HTTP_USER));
    }

    /**
     * Get auth password
     * @return string
     */
    public function getHttpAuthPassword()
    {
        return trim(Mage::getStoreConfig(self::XML_PATH_HTTP_PASS));
    }

    /**
     * Get FTP user
     * @return string
     */
    public function getFtpUser()
    {
        return trim(Mage::getStoreConfig(self::XML_PATH_FTP_USER));
    }

    /**
     * Get FTP pass
     * @return string
     */
    public function getFtpPass()
    {
        return trim(Mage::getStoreConfig(self::XML_PATH_FTP_PASS));
    }

    /**
     * Get FTP path
     * @return string
     */
    public function getFtpPath()
    {
        return trim(Mage::getStoreConfig(self::XML_PATH_FTP_PATH));
    }

    /**
     * Get FTP host
     * @return string
     */
    public function getFtpHost()
    {
        return trim(Mage::getStoreConfig(self::XML_PATH_FTP_HOST));
    }

    /**
     * Get FTP port
     * @return int
     */
    public function getFtpPort()
    {
        return intval(trim(Mage::getStoreConfig(self::XML_PATH_FTP_PORT))) ?: 21;
    }

    /**
     * Get SSL mode
     * @return bool
     */
    public function getFtpSsl()
    {
        return (bool) Mage::getStoreConfig(self::XML_PATH_FTP_SSL);
    }

    /**
     * Get passive mode
     * @return bool
     */
    public function getFtpPassive()
    {
        return (bool) Mage::getStoreConfig(self::XML_PATH_FTP_PASSIVE);
    }

    /**
     * Is ftp enabled
     * @return bool
     */
    public function isFtpEnabled()
    {
        if (!Mage::getStoreConfig(self::XML_PATH_FTP_ENABLED)) {
            return false;
        }

        return ($this->getFtpUser() && $this->getFtpPass() && $this->getFtpHost());
    }

    /**
     * Is newsletter feed enabled
     * @return bool
     */
    public function isNewsletterFeedEnabled()
    {
        return (bool) Mage::getStoreConfig(self::XML_PATH_NEWSLETTER_ENABLED);

    }

    /**
     * Get root path ids
     * @param int $storeId
     * @return array|null
     */
    public function getRootPathIds($storeId)
    {
        if (!isset($this->_rootPathIds[$storeId])) {
            $rootCategoryId = Mage::app()->getStore($storeId)->getRootCategoryId();
            $rootCategory = Mage::getModel('catalog/category')->load($rootCategoryId);

            $this->_rootPathIds[$storeId] = $rootCategory->getPathIds();
        }

        return $this->_rootPathIds[$storeId];
    }

    /**
     * Get first valid category
     * @param array $categoryIds
     * @param int|string $storeId
     * @return string
     */
    public function getFirstValidCategory($categoryIds, $storeId)
    {
        $rootCategories = Mage::helper('adabra_feed')->getRootPathIds($storeId);
        $mainCategoryId = Adabra_Feed_Model_Feed_Category::FAKE_CATEGORY_ID;
        foreach ($categoryIds as $categoryId) {
            if (!in_array($categoryId, $rootCategories)) {
                $mainCategoryId = $categoryId;
                break;
            }
        }

        return $mainCategoryId;
    }

    /**
     * Get a list of product custom attribute (tags)
     * @return array
     */
    public function getCustomTagsList()
    {
        $result = [];
        $listAttributes = trim(Mage::getStoreConfig(self::XML_PATH_GENERAL_CUSTOM_TAGS));
        if($listAttributes != "") {
            $result = preg_split('/[\r\n]+/', $listAttributes);
        }
        return $result;
    }

    /**
     * get Customer Address Type to export
     * @return string
     */
    public function getAddressType()
    {
        return Mage::getStoreConfig(self::XML_PATH_CUSTOMER_ADDRESS_TYPE);

    }

    /**
     * @param null $storeId
     * @return bool
     */
    public function isCustomerFeedEnabled($storeId = null)
    {
        return (bool) Mage::getStoreConfig(self::XML_PATH_CUSTOMER_ENABLED, $storeId);

    }

    /**
     * @return string
     */
    public function getExtensionVersion()
    {
        return (string) Mage::getConfig()->getNode()->modules->Adabra_Feed->version;
    }

    /**
     * Send Api call to Adabra to notify ending of feed generation
     * @param $feedType
     * @param null $idPortale
     * @param null $idCatalogo
     * @return curl result
     */

    public function sendApiCallToAdabra($feedType, $idPortale = null, $idCatalogo = null)
    {
        $action = '';
        switch ($feedType) {
            case 'product':
                $action = self::CATALOG_ACTION;
                break;
            case 'customer':
                $action = self::CUSTOMER_ACTION;
                break;
            case 'order':
                $action = self::ORDER_ACTION;
                break;
            case 'subscriber':
                $action = self::NEWSLETTER_ACTION;
                break;
        }
        $sendingData = [];
        $tmpHash = '';
        $sendingData['idCatalogo'] = $idCatalogo;
        $sendingData['idPortale'] = $idPortale;

        ksort($sendingData);

        foreach($sendingData as $key => $value) {
            $tmpHash .= $key . json_encode($value);
        }
        $magentoSignature  = md5($tmpHash . self::API_MAGENTO_SIGNATURE_KEY);
        $sendingData['magentoSignature'] = $magentoSignature;

        $curl = curl_init();
        $baseUrl = $this->isApiStagingModeEnable() ? self::BASE_URL_STAGING : self::BASE_URL_PRODUCTION;
        curl_setopt_array($curl, array(
            CURLOPT_URL => $baseUrl.self::RELATIVE_ENDPOINT.$action,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($sendingData,true),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return $err ? json_decode($err) : json_decode($response);

    }

    /**
     * Is Api notify enabled
     * @return bool
     */
    public function isApiNotifyEnable()
    {
        return (bool) Mage::getStoreConfig(self::XML_PATH_GENERAL_API_NOTIFY);
    }

    /**
     * Is Api staging mode enabled
     * @return bool
     */
    public function isApiStagingModeEnable()
    {
        return (bool) Mage::getStoreConfig(self::XML_PATH_GENERAL_API_STAGING_MODE);
    }


    /**
     * @param null $storeId
     * @return bool
     */
    public function isIncrementalFeedEnabled ($storeId = null)
    {
        return (bool) Mage::getStoreConfig(self::XML_PATH_GENERAL_INCREMENTAL_ENABLED, $storeId);
    }

    /**
     * @param null $storeId
     * @return bool
     */
    public function isOrderFilterEnable($storeId = null)
    {
        return (bool) Mage::getStoreConfig(self::XML_PATH_ORDER_EXCLUDE_ORDER, $storeId);

    }

    /**
     * get Order Exclusion Filter List of Expression
     * @param null $storeId
     * @return string
     */
    public function getOrderFilterList($storeId = null)
    {
        return Mage::getStoreConfig(self::XML_PATH_ORDER_EXCLUDE_ORDER_FILTER_LIST, $storeId);

    }

    /**
     * Is Logging  mode enabled
     * @return bool
     */
    public function isLoggingModeEnable()
    {
        return (bool) Mage::getStoreConfig(self::XML_PATH_GENERAL_ENABLE_LOGGING_MODE);
    }

    /**
     * @param $desc
     * @param $val
     */
    public function adabraLog($desc, $val) {

        if ($this->isLoggingModeEnable()) {
            Mage::log($desc, $val, self::ADABRA_LOG, true);
        }
    }
}
