<?php
/**
 * Aggiunto campo (DATATIME) sul database per la gestione degli update subscriber
 * - tabella newsletter_subscriber: adb_subscriber_last_update
 */

$installer = $this;
$connection = $installer->getConnection();

$installer->startSetup();

$newsletterSubscriberTable = $installer->getTable('newsletter_subscriber');
$adabraFeedsTable = $installer->getTable('adabra_feed');

$installer->getConnection()->addColumn(
    $newsletterSubscriberTable,
    'adb_subscriber_last_update',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'nullable'  => false,
        'comment'   => 'Adb - subscriber last update'
));

$installer->getConnection()->addColumn(
    $adabraFeedsTable,
    'category_feed_last_update',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'nullable'  => true,
        'comment'   => 'Category Feed last update'
    ));

$installer->getConnection()->addColumn(
    $adabraFeedsTable,
    'product_feed_last_update',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'nullable'  => true,
        'comment'   => 'Product Feed last update'
    ));

$installer->getConnection()->addColumn(
    $adabraFeedsTable,
    'customer_feed_last_update',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'nullable'  => true,
        'comment'   => 'Customer Feed last update'
    ));

$installer->getConnection()->addColumn(
    $adabraFeedsTable,
    'order_feed_last_update',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'nullable'  => true,
        'comment'   => 'Order Feed last update'
    ));

$installer->getConnection()->addColumn(
    $adabraFeedsTable,
    'subscriber_feed_last_update',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'nullable'  => true,
        'comment'   => 'Subscriber Feed last update'
    ));

$installer->endSetup();
