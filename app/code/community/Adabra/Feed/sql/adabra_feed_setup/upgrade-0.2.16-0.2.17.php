<?php
/**
 * Created by PhpStorm.
 * User: matteo
 * Date: 2019-02-14
 * Time: 15:59
 */

$installer = $this;
$connection = $installer->getConnection();

$installer->startSetup();

$tableName = $installer->getTable('adabra_feed/feed');

$installer->getConnection()->addColumn(
    $tableName, 'adabra_site_id', array(
    'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'nullable' => false,
    'after'     => 'store_id',
    'comment' => 'ID portale Adabra'
));

$installer->getConnection()->addColumn(
    $tableName, 'adabra_catalog_id', array(
    'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'nullable' => false,
    'after'     => 'adabra_site_id',
    'comment' => 'ID catalogo Adabra'
));

$installer->endSetup();
