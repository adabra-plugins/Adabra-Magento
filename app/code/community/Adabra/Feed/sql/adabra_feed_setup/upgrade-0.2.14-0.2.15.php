<?php

$installer = $this;
$connection = $installer->getConnection();

$installer->startSetup();

$tableName = $installer->getTable('adabra_feed/feed');

$installer->getConnection()->addColumn(
    $tableName,
    'status_subscriber',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => false,
        'length'    => 255,
        'after'     => 'status_customer', // column name to insert new column after
        'comment'   => 'Status Subscriber'
));

$installer->endSetup();