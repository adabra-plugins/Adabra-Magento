<?php
/**
 * Created by PhpStorm.
 * User: matteo
 * Date: 2019-01-10
 * Time: 12:35
 */

class Adabra_Feed_Model_Observer
{
    public function newsletterSubscriberSaveBefore($observer)
    {
        $subscriber = $observer->getEvent()->getSubscriber();
        $now = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
        $subscriber->setAdbSubscriberLastUpdate($now);

    }

}
