<?php
/**
 * MageSpecialist
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magespecialist.it so we can send you a copy immediately.
 *
 * @category   Adabra
 * @package    Adabra_Feed
 * @copyright  Copyright (c) 2017 Skeeller srl / MageSpecialist (http://www.magespecialist.it)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Adabra_Feed_Model_Feed_Subscriber extends Adabra_Feed_Model_Feed_Abstract
{
    protected $_type = 'subscriber';
    protected $_exportName = 'subscriber';
    protected $_scope = 'website';


    /**
     * Prepare collection
     */
    protected function _prepareCollection()
    {
        /*leggo tutte le store view associate al website*/
        $storeList = array();
        foreach ($this->getStore()->getWebsite()->getGroups() as $group) {
            $stores = $group->getStores();
            foreach ($stores as $store) {
                array_push($storeList,  $store->getStoreId());

            }
        }
        $this->_collection = Mage::getModel('newsletter/subscriber')->getCollection()
            ->addFieldToFilter('store_id', array ('in' => $storeList));
        if (Mage::helper('adabra_feed')->isIncrementalFeedEnabled()) {
            $subscriberFeedLastUpdate = $this->getFeed()->getSubscriberFeedLastUpdate();
            if(isset($subscriberFeedLastUpdate)) {
                $this->_collection->addFieldToFilter('adb_subscriber_last_update', array('gt' => $subscriberFeedLastUpdate));
            }
        }

    }

    /**
     * Get headers
     * @return array
     */
    protected function _getHeaders()
    {
        return array(
            'email',
            'f_ricevi_newsletter',
            'f_ricevi_newsletter_ts',
            'f_attivo',
            'liste_newsletter',
            'mantieni_liste_attuali',
            'unsubscribe_code'
        );
    }

    /**
     * Get feed row
     * @param Varien_Object $entity
     * @return array
     * @throws Mage_Core_Model_Store_Exception
     */
    protected function _getFeedRow(Varien_Object $entity)
    {
        /** @var Mage_Newsletter_Model_Subscriber $subscriber */
        $subscriber = $entity;
        $store = Mage::app()->getStore($subscriber->getStoreId());
        $status = $subscriber->getSubscriberStatus();
        $f_ricevi_newsletter = 0;
        $f_ricevi_newsletter_ts = $subscriber->getAdbSubscriberLastUpdate();
        $unsubscribe_code = $subscriber->getUnsubscriptionLink();
        $f_attivo = 0;
        switch($status) {

            case 1:
                $f_ricevi_newsletter = 1;
                $f_attivo = 1;
                break;

            case 2:
                $f_ricevi_newsletter = 0;
                $f_attivo = 0;
                break;

            case 3:
                $f_ricevi_newsletter = 0;
                $f_attivo = 1;
                break;

            case 4:
                $f_ricevi_newsletter = 0;
                $f_attivo = 0;
                break;
        }
        return array(array(
            $subscriber->getEmail(),
            $this->_toBoolean($f_ricevi_newsletter),
            $f_ricevi_newsletter_ts,
            $this->_toBoolean($f_attivo),
            $store->getWebsite()->getCode()."_".$store->getCode(),
            $this->_toBoolean(true),
            $unsubscribe_code
        ));
    }
}
