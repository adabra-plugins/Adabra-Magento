<?php
/**
 * MageSpecialist
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magespecialist.it so we can send you a copy immediately.
 *
 * @category   Adabra
 * @package    Adabra_Feed
 * @copyright  Copyright (c) 2017 Skeeller srl / MageSpecialist (http://www.magespecialist.it)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Adabra_Feed_Model_Feed_Customer extends Adabra_Feed_Model_Feed_Abstract
{
    protected $_type = 'customer';
    protected $_exportName = 'customers';
    protected $_scope = 'website';
    protected $_virtualFields = array();


    /**
     * Get virtual field model
     * @param $fieldName
     * @return Adabra_Feed_Model_Source_Vfield
     */
    protected function _getVirtualFieldModel($fieldName)
    {
        if (!isset($this->_virtualFields[$fieldName])) {
            $this->_virtualFields[$fieldName] = Mage::getModel('adabra_feed/vfield')
                ->getCollection()
                ->addFieldToFilter('vfield_type', ['eq' => Adabra_Feed_Model_Source_Vfield_Type::TYPE_CUSTOMER])
                ->addFieldToFilter('code', ['eq' => $fieldName])
                ->getFirstItem();
        }

        return $this->_virtualFields[$fieldName];
    }

    /**
     * Get virtual field
     * @param Mage_Customer_Model_Customer $customer
     * @param $fieldName
     * @return string
     */
    protected function _getVirtualField(Mage_Customer_Model_Customer $customer, $fieldName)
    {
        $fieldModel = $this->_getVirtualFieldModel($fieldName);
        return $fieldModel->getCustomerValue($customer);
    }

    /**
     * Return true if $customer is a newsletter subscirber
     * @param Mage_Customer_Model_Customer $customer
     * @return boolean
     */
    protected function _getIsNewsletterSubscriber(Mage_Customer_Model_Customer $customer)
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $websiteId = $customer->getWebsiteId();
        $website = Mage::getModel('core/website')->load($websiteId);
        $storeList = array();
        $i = 0;
        foreach ($website->getGroups() as $group) {
            $stores = $group->getStores();
            foreach ($stores as $store) {
                $storeList[$i] = $store->getId();
                $i++;
            }
        }
        $tableName = $resource->getTableName('newsletter_subscriber');
        $qry = $readConnection->select()->from($tableName, 'subscriber_id')
            ->where('subscriber_email = ' . $readConnection->quote($customer->getEmail()))
            ->where('subscriber_status in (1,3)')
            ->where('store_id in '.'('. implode(', ', $storeList).')')
            ->limit(1);

        return $readConnection->fetchOne($qry) ? true : false;
    }

    /**
     * Return dateTime of last update if $customer is a newsletter subscriber
     * @param Mage_Customer_Model_Customer $customer
     * @return string
     */
    protected function _getLastUpdateNewsletterSubscriber(Mage_Customer_Model_Customer $customer)
    {
        $subscriber = Mage::getModel('newsletter/subscriber')->loadByCustomer($customer);

        return ($subscriber) ? $subscriber->getAdbSubscriberLastUpdate() : '';
    }

    /**
     * Return unsubscriber link if $customer is a newsletter subscriber
     * @param Mage_Customer_Model_Customer $customer
     * @return string
     */
    protected function _getUnsubscribeLinkForNewsletterSubscriber(Mage_Customer_Model_Customer $customer)
    {
        $subscriber = Mage::getModel('newsletter/subscriber')->loadByCustomer($customer);

        return ($subscriber) ? $subscriber->getUnsubscriptionLink() : '';
    }

    /**
     * Return subscription status if $customer is a newsletter subscriber
     * @param Mage_Customer_Model_Customer $customer
     * @return string
     */
    protected function _getStatusForNewsletterSubscriber(Mage_Customer_Model_Customer $customer)
    {
        $subscriber = Mage::getModel('newsletter/subscriber')->loadByCustomer($customer);

        return $subscriber->getStatus();
    }

    /**
     * Prepare collection
     * @throws Mage_Core_Exception
     */
    protected function _prepareCollection()
    {
        $this->_collection = Mage::getModel('customer/customer')->getCollection();
        $this->_collection
            ->addAttributeToSelect('*')
            ->addFieldToFilter('email', array ('nlike'=>'%@marketplace.amazon%'))
            ->addFieldToFilter('website_id', $this->getStore()->getWebsiteId());

        if (Mage::helper('adabra_feed')->isIncrementalFeedEnabled()) {

            $customerFeedLastUpdate = $this->getFeed()->getCustomerFeedLastUpdate();

            if (isset($customerFeedLastUpdate)) {

                $customerAddressEntity = Mage::getSingleton('core/resource')->getTableName('customer_address_entity');
                $newsletterSubscriber = Mage::getSingleton('core/resource')->getTableName('newsletter_subscriber');

                /** join sulla tabella customer_address_entity **/

                $this->_collection->joinTable(
                    array('address' => $customerAddressEntity), 'parent_id=entity_id',
                    array('updated_at' => 'updated_at'), null, 'left')
                    ->joinTable(
                        array('subscriber' => $newsletterSubscriber), 'customer_id=entity_id',
                        array('adb_subscriber_last_update' => 'adb_subscriber_last_update'), null, 'left');

                /** controllo se il cliente ha aggiornato i dati anagrafici (customer_entity) oppure un indirizzo (customer_address_entity)
                 *  oppure i dati relativi alle subscription newsletter (newsletter_subscriber)
                 */

                $this->_collection->getSelect()->where(
                    '(e.updated_at > \'' . $customerFeedLastUpdate . '\') 
                            OR (address.updated_at > \'' . $customerFeedLastUpdate . '\' )
                            OR (subscriber.adb_subscriber_last_update > \'' . $customerFeedLastUpdate . '\' )');

                /** group by entity_id sulla tabella customer_entity **/

                $this->_collection->getSelect()->group('e.entity_id');
            }
        }


    }

    /**
     * Get headers
     * @return array
     */
    protected function _getHeaders()
    {
        return array(
            'id_utente',
            'email',
            'nome',
            'cognome',
            'citta',
            'cap',
            'indirizzo',
            'provincia',
            'regione',
            'stato',
            'cellulare',
            'telefono',
            'sesso',
            'nascita_anno',
            'nascita_mese',
            'nascita_giorno',
            'f_business',
            'azienda_nome',
            'azienda_categoria',
            'f_ricevi_newsletter',
            'f_ricevi_newsletter_ts',
            'f_ricevi_comunicazioni_commerciali',
            'data_iscrizione',
            'data_cancellazione',
            'ip',
            'user_agent',
            'f_attivo',
            'f_cancellato',
            'fidelity_card',
            'unsubscribe_code'
        );
    }

    /**
     * Get feed row
     * @param Varien_Object $entity
     * @return array
     */
    protected function _getFeedRow(Varien_Object $entity)
    {
        /** @var Mage_Customer_Model_Customer $customer */
        $customer = $entity;

        $addressType = Mage::helper('adabra_feed')->getAddressType();
        if ($addressType == Mage_Customer_Model_Address_Abstract::TYPE_BILLING) {
            $address = $customer->getDefaultBillingAddress();
        } else if ($addressType == Mage_Customer_Model_Address_Abstract::TYPE_SHIPPING) {
            $address = $customer->getDefaultShippingAddress();
        }

        $dob = preg_split('/\D+/', $customer->getDob());

        if (!isset($dob[0])) {
            $dob[0] = '';
        }

        if (!isset($dob[1])) {
            $dob[1] = '';
        }

        if (!isset($dob[2])) {
            $dob[2] = '';
        }
        $isNewsletterSubscriberTable = $this->_getIsNewsletterSubscriber($customer);
        $subscriptionStatus = false;
        if($isNewsletterSubscriberTable){
            $subscriptionStatus = $this->_getStatusForNewsletterSubscriber($customer);
            if($subscriptionStatus == Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED){
                // iscritto NL
                $subscriptionStatus = true;
            }
            else if($subscriptionStatus == Mage_Newsletter_Model_Subscriber::STATUS_UNSUBSCRIBED){
                // discritto NL
                $subscriptionStatus = false;
            }
        }


        return array(array(
            $customer->getId(),
            $customer->getEmail(),
            $customer->getFirstname(),
            $customer->getLastname(),
            $address ? $address->getCity() : '',
            $address ? $address->getPostcode() : '',
            $address ? $address->getStreetFull() : '',
            '',
            $address ? $address->getRegionCode() : '',
            $address ? $address->getCountry() : '',
            '',
            $address ? $address->getTelephone() : '',
            $customer->getGender() == 2 ? 'f' : 'm',
            $dob[0],
            $dob[1],
            $dob[2],
            $this->_getVirtualField($customer, 'f_business'),
            $address ? $address->getCompany() : '',
            $this->_getVirtualField($customer, 'azienda_categoria'),
            $isNewsletterSubscriberTable ? $this->_toBoolean($subscriptionStatus) : $this->_toBoolean(false),
            $isNewsletterSubscriberTable ? $this->_getLastUpdateNewsletterSubscriber($customer) : '',
            $this->_toBoolean($this->_getVirtualField($customer, 'f_ricevi_comunicazioni_commerciali')),
            $this->_toTimestamp2($customer->getCreatedAtTimestamp()),
            '',
            '',
            '',
            $this->_toBoolean(true),
            $this->_toBoolean(false),
            $this->_getVirtualField($customer, 'fidelity_card'),
            $subscriptionStatus ? $this->_getUnsubscribeLinkForNewsletterSubscriber($customer) : '',
        ));
    }
}
