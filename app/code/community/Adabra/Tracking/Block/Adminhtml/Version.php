<?php
/**
 * Created by PhpStorm.
 * User: matteo
 * Date: 2018-12-31
 * Time: 12:11
 */

class Adabra_Tracking_Block_Adminhtml_Version extends Mage_Adminhtml_Block_System_Config_Form_Field{

protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        return (string) Mage::helper('adabra_tracking')->getExtensionVersion();
    }
}