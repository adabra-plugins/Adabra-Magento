<?php
/**
 * MageSpecialist
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magespecialist.it so we can send you a copy immediately.
 *
 * @category   Adabra
 * @package    Adabra_Common
 * @copyright  Copyright (c) 2017 Skeeller srl / MageSpecialist (http://www.magespecialist.it)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Adabra_Common_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Recursively implodes an array with optional key inclusion
     *
     * Example of $include_keys output: key, value, key, value, key, value
     *
     * @access  public
     * @param   array   $array         multi-dimensional array to recursively implode
     * @param   string  $glue          value that glues elements together
     * @param   bool    $include_keys  include keys before their values
     * @param   bool    $trim_all      trim ALL whitespace from string
     * @return  string  imploded array
     */
    public function recursive_implode(array $array, $glue = ',', $include_keys = false, $trim_all = true)
    {
        $glued_string = '';
        // Recursively iterates array and adds key/value to glued string
        array_walk_recursive($array, function($value, $key) use ($glue, $include_keys, &$glued_string)
        {
            $include_keys and $glued_string .= $key.$glue;
            $glued_string .= $value.$glue;
        });
        // Removes last $glue from string
        strlen($glue) > 0 and $glued_string = substr($glued_string, 0, -strlen($glue));
        // Trim ALL whitespace
        //$trim_all and $glued_string = preg_replace("/(\s)/ixsm", '', $glued_string);
        $glued_string = trim($glued_string);
        return (string) $glued_string;
    }

    /**
     * Get list of custom tag associated to product
     * @param Mage_Catalog_Model_Product $product
     * @return array
     * @throws Mage_Core_Exception
     */
    public function _getCustomTagsList(Mage_Catalog_Model_Product $product)
    {
        $tagList = [];
        $tagsListArray = Mage::helper('adabra_feed')->getCustomTagsList();
        foreach ($tagsListArray as $tag) {
            $tagToExport = $this->getTagToExport($product->getId(), $tag);
            if (!empty($tagToExport)) {
                array_push($tagList, $tagToExport);
            }

        }
        return $tagList;
    }

    /**
     * @param int $entityId
     * @param int|string|array $attribute attributes's ids or codes
     * @param null|int|Mage_Core_Model_Store $store
     *
     * @return null|string
     * @throws Mage_Core_Exception
     */
    public function getTagToExport($entityId, $attribute, $store=null) {
        if (!$store) {
            $store = Mage::app()->getStore();
        }
        $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product',$attribute);
        $attributeType = $attributeModel->getFrontendInput();
        $label =  $attributeModel->getStoreLabel($store->getId());
        $value = (string)Mage::getResourceModel('catalog/product')->getAttributeRawValue($entityId, $attribute, $store);
        if (!empty($value)) {
            $tag = '';
            switch ($attributeType) {
                /* per gli attributi boolean si riporta solamente la label in caso l'attributo sia true */
                case "boolean":
                    if ($value == 1) {
                        $tag = $attributeModel->getStoreLabel($store->getId());
                    }
                    break;
                case "text":
                    $tag = $label.":".$value;
                    break;

                case "textarea":
                    $tag = $label.":".$value;
                    break;

                case "select":
                    $tag = $label.":".Mage::getModel('catalog/product')->getResource()->getAttribute($attribute)->getSource()->getOptionText($value);
                    break;

                case "multiselect":
                    if(is_array(Mage::getModel('catalog/product')->getResource()->getAttribute($attribute)->getSource()->getOptionText($value))) {
                        $attributeList = $label . ":" . implode(",", Mage::getModel('catalog/product')->getResource()->getAttribute($attribute)->getSource()->getOptionText($value));
                        $tag = $attributeList;
                    }
                    else {
                        $tag = $label.":".Mage::getModel('catalog/product')->getResource()->getAttribute($attribute)->getSource()->getOptionText($value);
                    }

                    break;
            }
            return $tag = str_replace('|', '', $tag);
        }

        return null;
    }

    /**
     * @param $entityId
     * @param $attribute
     * @param null $store
     * @return mixed|null
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getProductAttributes($entityId, $attribute, $store=null) {
        if (!$store) {
            $store = Mage::app()->getStore();
        }
        $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product',$attribute);
        $attributeType = $attributeModel->getFrontendInput();
        $value = (string)Mage::getResourceModel('catalog/product')->getAttributeRawValue($entityId, $attribute, $store);
        if (!empty($value)) {
            switch ($attributeType) {
                case "boolean":
                    if ($value == 1) {
                        $attribute = $value;
                    }
                    break;
                case "textarea":
                case "text":
                    $attribute = $value;
                    break;

                case "select":
                    $attribute = Mage::getModel('catalog/product')->getResource()->getAttribute($attribute)->getSource()->getOptionText($value);
                    break;

                case "multiselect":
                    if(is_array(Mage::getModel('catalog/product')->getResource()->getAttribute($attribute)->getSource()->getOptionText($value))) {
                        $attributeList = implode(",", Mage::getModel('catalog/product')->getResource()->getAttribute($attribute)->getSource()->getOptionText($value));
                        $attribute = $attributeList;
                    }
                    else {
                        $attribute = Mage::getModel('catalog/product')->getResource()->getAttribute($attribute)->getSource()->getOptionText($value);
                    }

                    break;
            }
            return $attribute = str_replace('|', '', $attribute);
        }

        return null;
    }

}
